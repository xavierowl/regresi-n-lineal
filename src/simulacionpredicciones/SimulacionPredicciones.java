/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacionpredicciones;

import java.awt.BasicStroke;
import java.awt.Color;
import java.io.FileInputStream;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYErrorRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author Byron
 */
public class SimulacionPredicciones extends Application{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage stage){
        XYSeries datosReales = new XYSeries("Casos nuevos");
        XYSeries datos = new XYSeries("Casos nuevos (real)");
        //Variable independiente
        int[] x = new int[92];
        //Variable dependiente
        int[] y = {1,0,0,2,0,6,0,7,18,11,12,8,37,26,30,52,25,71,72,92,21,48,69,94,96,108,159,96,106,139,79,94,201,274,169,250,236,67,76,127,126,128,206,182,171,185,172,207,205,320,261,237,218,352,262,296,499,279,383,305,640,346,497,595,444,568,550,659,658,680,606,723,635,721,640,752,643,801,1046,998,806,1022,1101,1262,1322,1548,1147,1110,1340,1521,1766,1515};
        for (int i = 0; i < 92; i++) {
            x[i] = i+1;
        }
        /**
         * Suma de x
         */
        int sx = 0;
        /**
         * Suma de y
         */
        int sy = 0;
        /**
         * Suma de x*y
         */
        int sxy = 0;
        /**
         * Suma de x^2
         */
        int sx2 = 0;
        /**
         * Asignación de las sumatorias
         */
        for (int i = 0; i < 92; i++) {
            sx = sx + x[i];
            sy = sy + y[i];
            sxy = sxy + x[i]*y[i];
            sx2 = sx2 + x[i]*x[i];
        }
        /**
         * Obtenciónde la pendiente
         */
        double m = ((92*sxy)-(sx*sy))/((92*sx2)-sx2);
        /**
         * Obtención del componente b
         */
        double b = (sy/92)-(m*(sx/92));
        /**
         * Inserción de los datos y sus predicciones según la regresión
         */
        for (int i = 1; i < 93; i++) {
            datos.add(i, m*i+b);
            datosReales.add(i,y[i-1]);
        }
        
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Días");
        yAxis.setLabel("Casos confirmados");
        //creating the chart
        final LineChart<Number,Number> lineChart = 
                new LineChart<Number,Number>(xAxis,yAxis);
        lineChart.setTitle("Gráfica de los casos confirmados en Colombia");
        //defining a datosSerie
        XYChart.Series datosSerie = new XYChart.Series();
        XYChart.Series datosRealesSerie = new XYChart.Series();
        datosSerie.setName("Casos confirmados en Colombia (Regresión matemática)");
        datosRealesSerie.setName("Casos confirmados en Colombia (Realidad)");
        //populating the datosSerie with data
        
        for (int i = 1; i < 93; i++) {
                datosRealesSerie.getData().add(new XYChart.Data(i,y[i-1]));
            }
        
        datosSerie.getData().add(new XYChart.Data(1, m*1+b));
        datosSerie.getData().add(new XYChart.Data(150, m*150+b));
        
        Scene scene  = new Scene(lineChart,800,600);
        lineChart.getData().add(datosSerie);
        lineChart.getData().add(datosRealesSerie);
       
        stage.setScene(scene);
        stage.setTitle("Casos confirmados en Colombia");
        stage.show();
        }
}
